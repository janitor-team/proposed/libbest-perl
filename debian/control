Source: libbest-perl
Section: perl
Testsuite: autopkgtest-pkg-perl
Rules-Requires-Root: no
Priority: optional
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: gregor herrmann <gregoa@debian.org>
Build-Depends: debhelper-compat (= 13), libmodule-install-perl
Build-Depends-Indep: libtest-exception-perl,
 libtest-pod-perl,
 perl
Standards-Version: 4.1.3
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libbest-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libbest-perl.git
Homepage: https://metacpan.org/release/Best

Package: libbest-perl
Architecture: all
Depends: ${misc:Depends}, ${perl:Depends}
Description: fallbackable module loader
 Best.pm attempts to load Perl modules from a list, stopping at the first
 successful load and failing only if no alternative was found.
 .
 Often there are several possible providers of some functionality your program
 needs, but you don't know which is available at the run site. For example,
 one of the modules may be implemented with XS, or not in the core Perl
 distribution and thus not necessarily installed. With Best.pm, this can
 be as simple as:
 .
  use Best qw/YAML::Syck YAML/;
